@extends('layouts.profile')
@section('title', 'Предложить проект')
@section('content')
    <!-- inventor begin-->
    <div class="inventor" style="padding:30px;">
        <div class="container">
            <div class="row">
                @include('partials.inform')
                <form action="{{ route('profile.projects.store') }}" enctype="multipart/form-data" method="POST" target="_top" style="width:100%;">
                    {{ csrf_field() }}

                    <h4>Подтвердите корректность введенных данных:</h4>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Название проекта <strong>*</strong></label>
                        <div class="">
                            <input type="text" name="name" class="form-control" value="{{ $project->name }}" readonly>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Логотип <strong>*</strong></label>
                        <div class="">
                            <img src="/project_img/{{ $project->logotype_url }}" style="max-width: 160px; max-height: 120px;">
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Контакты куратора (имя, телеграм логин) <strong>*</strong></label>
                        <div class="">
                            <textarea name="curator_contacts" class="form-control" readonly>{{ $project->curator_contacts }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Ссылка на проект</label>
                        <div class="">
                            <input type="text" name="url" class="form-control" value="{{ $project->url }}" readonly>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Ссылка на видео-описание</label>
                        <div class="">
                            <input type="text" name="video_description" class="form-control" value="{{ $project->video_description }}" readonly>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Краткое описание проекта <strong>*</strong></label>
                        <div class="">
                            <textarea name="description" class="form-control" readonly>{{ $project->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="wallet">Маркетинг план <strong>*</strong></label>
                        <div class="">
                            <textarea name="marketing_description" class="form-control" readonly>{{ $project->marketing_description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-5">
                        <label class="control-label" for="currency" style="font-weight: bold">Оплатить через</label>
                        <div class="">
                            <select id="currency" name="currency" class="form-control" autofocus>
                                @foreach(App\Models\PaymentSystem::where('connected', 1)->get() as $paymentSystem)
                                    @foreach($paymentSystem->currencies as $currency)
                                        <option value="{{ $paymentSystem->id.':'.$currency->id }}">{{ $paymentSystem->name }} {{ $currency->code }}</option>
                                    @endforeach
                                @endforeach
                            </select>

                            <p class="help" style="font-style: italic;">Публикация предложения проекта на сайте, стоит <strong>{{ env('PROJECT_OFFER_COST', 85) }} USD</strong></p>
                        </div>
                    </div>

                    <script>
                        function goBack() {
                            window.history.back();
                        }
                    </script>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Перейти к оплате
                                </button>
                                <button type="button" onClick="goBack();" class="btn btn-warning" style="margin-left:15px;">
                                    Редактировать введенные данные
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- inventor end -->
    <!-- contact end -->
@endsection