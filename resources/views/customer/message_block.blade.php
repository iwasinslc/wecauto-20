@extends('layouts.auth')

@section('title', __('Email verification'))
@section('content')
    <div class="auth__content">
        <div class="auth-module auth-module--not-content">
            <div class="auth-module__form">
                <h2 style="text-align: center;"><strong>{{ __($message) }}</strong></h2>
            </div>
        </div>
    </div>
@endsection
