<?php
namespace App\Console\Commands\Automatic;

use App\Events\NotificationEvent;
use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\Currency;
use App\Models\DepositQueue;
use App\Models\ExchangeOrder;
use App\Models\Transaction;
use App\Models\Wallet;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class DepositQueueCommand
 * @package App\Console\Commands\Automatic
 */
class StashOrdersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stash:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposits queues.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        $orders = ExchangeOrder::active()->where('currency_id', Currency::getByCode('ACC')->id)->where('type', ExchangeOrder::TYPE_BUY)->get();
        /**
         * @var ExchangeOrder $order
         */
        foreach ($orders as $order) {

            /**
             * @var User $user
             */
            $user = $order->user;

            /**
             * @var Wallet $wallet
             */
            $wallet = $user->getUserWallet('FST');




            $percent = 50;



            $bonus = $order->amount*$order->rate*$percent*0.01*rate('USD', 'FST');

            $transaction = Transaction::order_stash($wallet, $bonus, $order);

            if ($transaction!==null)
            {
                $wallet->addAmountWithoutAccrueToPartner($bonus);

                NotificationEvent::dispatch($order->user, 'notifications.parking_bonus', [
                    'id'=>$order->id,
                    'user_id'=>$order->user_id,
                    'amount'=>number_format($bonus, 8),
                    'currency'=>$wallet->currency->code
                ]);
            }



        }
    }
}
