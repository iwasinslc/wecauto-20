<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestUserSearch
 * @package App\Http\Requests\Api
 * @property string login
 * @property string wallet_address
 * @property string batch_id
 */
class RequestUserSearch extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login'             => 'required_without_all:wallet_address,batch_id|max:150',
            'wallet_address'    => 'string|max:191',
            'batch_id'          => 'sometimes'
        ];
    }
}
