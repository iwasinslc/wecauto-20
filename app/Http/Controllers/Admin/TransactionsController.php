<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

/**
 * Class TransactionsController
 * @package App\Http\Controllers\Admin
 */
class TransactionsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $types = TransactionType::get();
        return view('admin.transactions.index', [
            'types' => $types
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function approveMany(Request $request)
    {
        $messages = [];

        if ($request->approve && $request->list) {
            foreach ($request->list as $item) {
                $messages[] = $this->approve($item, true);
            }
        } elseif ($request->delete && $request->list) {
            foreach ($request->list as $item) {
                $messages[] = $this->delete($item, true);
            }
        } else {
            return back()->with('error', __('Empty items list'));
        }

        return back()->with('success', __('List of withdrawal requests processed.').'<hr>'.implode('<hr>', $messages));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $transactions = Transaction::with('user', 'currency', 'type')
            ->select('transactions.*');

        return Datatables::of($transactions)
            ->addColumn('type_name', function ($transaction) {
                return __($transaction->type->name);
            })->editColumn('amount', function ($transaction) {
                return number_format($transaction->amount, $transaction->currency->precision, '.', '');
            })
            ->addColumn('partner_from', function ($transaction) {

                if ($transaction->type->name == 'transfer_send'||$transaction->type->name == 'transfer_receive') {

                    $user = User::where('email', $transaction->result)->first();
                    return $user!==null ?$user->login : __('empty');
                }
                if ($transaction->type->name != 'partner') {
                    return __('not affiliate');
                }

                $wallet = Wallet::where('id', $transaction->source)->first();

                if (null === $wallet) {
                    return __('source not found');
                }

                return $wallet->user->login;
            })
            ->make(true);
    }

    /**
     * @param Transaction $transaction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Transaction $transaction)
    {
        return view('admin.transactions.show', [
            'transaction' => $transaction
        ]);
    }


    /**
     * @param $id
     * @param bool $massMode
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id, $massMode=false)
    {
        /**
         * @var Transaction $transaction
         */
        $transaction = Transaction::find($id);

        if ($transaction->isApproved()) {
            if (true === $massMode) {
                return __('This request already processed.');
            }
            return back()->with('error', __('This request already processed.'));
        }


        if ($transaction->type->name=='enter')
        {
            $commission = $transaction->amount * 0.01 * $transaction->commission;
            $wallet = $transaction->wallet;
            if ($transaction->currency->code=='USD')
            {
                $wallet = $transaction->user
                    ->wallets()
                    ->where('payment_system_id', PaymentSystem::getByCode('perfectmoney')->id)
                    ->where('currency_id', $transaction->currency_id)
                    ->first();
            }

            $wallet->refill(($transaction->amount-$commission), $transaction->source);
        } elseif ($transaction->type->name=='penalty') {
            $transaction->wallet->approvePenalty($transaction->amount); // Approve penalty
        }
        $transaction->approved = 1;
        $transaction->save();

        return back()->with('success', __('Transaction successful approved'));
    }


    /**
     * @param $id
     * @param bool $massMode
     * @return array|\Illuminate\Http\RedirectResponse|string|null
     * @throws \Exception
     */
    public function delete($id, $massMode=false)
    {
        /**
         * @var Transaction $transaction
         */
        $transaction = Transaction::find($id);

        if ($transaction===null) {
            if (true === $massMode) {
                return __('This request already processed.');
            }
            return back()->with('error', __('This request already processed.'));
        }

        $transaction->delete();

        if (true === $massMode) {
            return __('Transaction deleted');
        }
        return back()->with('success', __('Transaction successful deleted'));
    }



}
