<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('currency_id');
            $table->string('rate_id');
            $table->string('user_id');
            $table->string('wallet_id');
            $table->string('name')->nullable();
            $table->float('daily')->default(0)->unsigned()->nullable();
            $table->float('overall')->default(0)->unsigned()->nullable();
            $table->integer('duration')->default(0)->unsigned()->nullable();
            $table->float('payout')->default(0)->unsigned()->nullable();
            $table->float('invested')->default(0)->unsigned();
            $table->float('balance', 24, 12)->default(0)->unsigned()->nullable();
            $table->boolean('reinvest')->default(0);
            $table->boolean('autoclose')->default(0);
            $table->boolean('active')->default(0);
            $table->string('condition')->default('undefined');
            $table->text('log')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
